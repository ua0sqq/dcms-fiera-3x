<?php

define('H', $_SERVER['DOCUMENT_ROOT'] . '/');

ob_start();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html" />
    <meta name="author" content="admin" />
    <title>
      Инсталяция Fiera
    </title>
    <link href="/install/css/style.css" type="text/css" rel="stylesheet" media="all"/>
  </head>
  <body>
<?php

$mysqli = true;
$install_mod = true;

include_once 'inc/fnc.php';

$step = $_SESSION['step_install'] = isset($_GET['step']) ? intval($_GET['step']) : 0;

?>
<div class="head">
    <img src="img/logotype.png" alt="*"/>
</div>
<?php

if (!install() || $step == 4)
{

?>
<div class="div">
<?php

    echo ($step != 0 ? '<img src="img/b1.png" alt="*"/> Приветствие и Соглашение' :
        '<img src="img/b2.png" alt="*"/> <strong>Приветствие и Соглашение</strong> ') .
        '<br />' . PHP_EOL;
    echo ($step != 1 ? '<img src="img/b1.png" alt="*"/> Проверка совместимости системы с хостингом' :
        '<img src="img/b2.png" alt="*"/> <strong>Проверка совместимости системы с хостингом</strong> ') .
        '<br />' . PHP_EOL;
    echo ($step != 2 ? '<img src="img/b1.png" alt="*"/> Установка CMOD ' :
        '<img src="img/b2.png" alt="*"/> <strong>Установка CMOD</strong> ') . '<br />' .
        PHP_EOL;
    echo ($step != 3 ? '<img src="img/b1.png" alt="*"/> Загрузка таблиц в базу данных ' :
        '<img src="img/b2.png" alt="*"/> <strong>Загрузка таблиц в базу данных</strong> ') .
        '<br />' . PHP_EOL;
    echo ($step != 4 ? '<img src="img/b1.png" alt="*"/> Регистрация Создателя ' :
        '<img src="img/b2.png" alt="*"/> <strong>Регистрация Создателя</strong>') .
        '<br />' . PHP_EOL;
    echo '</div>' . PHP_EOL;
} else
{

?>
<div class="msg" style="text-align:center;color:red;">
    <p><?= install()?></p>
</div>
<div class="foot">
    <a class="c1" href="//dcms-fiera.ru/?copyright">DCMS-FIERA</a>
</div>
</body>
</html>
<?php

    exit();
}
if (isset($_GET['type']))
    $module_loads = $_GET['type'];
else
    $module_loads = null;
if (preg_match('/\.php$/i', $module_loads))
    $module_s = true;
else
    $module_s = false;
if (is_file(H . 'install/page/' . $module_loads . '.php') and $module_s == false)
{
    include_once 'page/' . $module_loads . '.php';
} else
{
    include_once 'page/index.php';
}
ob_end_flush();

?>
<div class="foot">
    <a class="c1" href="//dcms-fiera.ru/?copyright">DCMS-FIERA</a>
</div>
</body>
</html>