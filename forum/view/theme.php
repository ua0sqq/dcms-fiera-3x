<?php

$forum = mysql_fetch_object(mysql_query('SELECT * FROM `forum` WHERE `id` = '.intval($_GET['forum'])));
$razdel = mysql_fetch_object(mysql_query('SELECT * FROM `forum_razdels` WHERE `id_forum` = '.$forum->id.' AND `id` = '.intval($_GET['razdel'])));
$theme = mysql_fetch_object(mysql_query('SELECT * FROM `forum_themes` WHERE `id_razdel` = '.$razdel->id.' AND `id` = '.intval($_GET['theme'])));

//для сео META данные
#-----------------------------------------------------------------------------#
$set['meta_keywords'] = meta_out_keywords(mb_substr($theme->description, 0, 160));
$_forum = $forum->name != null ? ','.$forum->name : null;
$_razdel = $razdel->name != null ? ','.$razdel->name : null;
$set['meta_description'] = meta_out_description($theme->name.''.$_forum.''.$_razdel);
#-----------------------------------------------------------------------------#

if (!$theme || !$razdel || !$forum || ($forum->access == 1 && $user['group_access'] < 8) || ($forum->access == 2 && $user['group_access'] < 3)) {
    header('Location: '.FORUM);
    exit;
} else {


    $set['title'] = 'Тема - '.output_text($theme->name, 1, 1, 0, 0, 0);
    include_once '../sys/inc/thead.php';
    title().aut();

    $my_report = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_reports` WHERE `id_theme` = '.$theme->id.' AND `id_user` = '.$user['id']), 0);
    $k_post = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_posts` WHERE `id_theme` = '.$theme->id), 0);

    if (mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_votes` WHERE `id_theme` = '.$theme->id), 0) != 0) {
        $vote = mysql_fetch_object(mysql_query('SELECT * FROM `forum_votes` WHERE `id_theme` = '.$theme->id));
        $vars = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_votes_var` WHERE `id_theme` = '.$theme->id), 0);
    }
    if (isset($_GET['sort_0'])) {
        unset($_SESSION['sort_'.$user['id']]);
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        exit;
    } elseif (isset($_GET['sort_1'])) {
        $_SESSION['sort_'.$user['id']] = 1;
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        exit;
    } elseif (isset($_GET['download'])) {
        $file = mysql_fetch_object(mysql_query('SELECT `id`, `id_post`, `name`, `count_downloads` FROM `forum_post_files` WHERE `id` = '.intval($_GET['download'])));
        $this_post = mysql_fetch_object(mysql_query('SELECT `hide`, `privat` FROM `forum_posts` WHERE `id` = '.$file->id_post));
        if ($this_post->privat == 0 || ($this_post->privat == $user['id'] || $this_post->id_user == $user['id']) || ($this_post->hide != 0 && user_access('forum_post_ed'))) {
            mysql_query('UPDATE `forum_post_files` SET `count_downloads` = '.($file->count_downloads+1).' WHERE `id` = '.$file->id);
            header('Location: '.FORUM.'/files/'.$file->name);
        } else {
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        }
        exit;
    } elseif (isset($user) && isset($_GET['cleare_theme']) && user_access('forum_them_edit')) {
        include_once 'action/clear_theme.php'; // Очистка темы.
    } elseif (isset($user) && isset($_GET['edit_theme'])) {
        include_once 'action/edit_theme.php'; // Редактирование темы.
    } elseif (isset($user) && isset($_GET['close_theme']) && $theme->reason_close == NULL && user_access('forum_them_edit')) {
        include_once 'action/close_theme.php'; // Закрытие темы.
    } elseif (isset($user) && isset($_GET['open_theme']) && $theme->reason_close != NULL && user_access('forum_them_edit')) {
        $_SESSION['success'] = '<div class = "msg">Тема успешно открыта.</div>';
        $msg_sys = 'Проблема решена. [url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] открыл тему.';
        mysql_query('INSERT INTO `forum_posts` SET `id_theme` = '.$theme->id.', `id_user` = "0", `id_admin` = "0", `text` = "'.mysql_real_escape_string($msg_sys).'", `time` = '.$time);
        mysql_query('UPDATE `forum_themes` SET `reason_close` = "" WHERE `id` = '.$theme->id);
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        exit;
    } elseif (isset($user) && isset($_GET['create_vote']) && ($theme->reason_close == NULL || ($theme->reason_close != NULL && user_access('forum_post_close'))) && $user['id'] == $theme->id_user) {
        include_once 'action/create_vote.php'; // Создание голосования.
    } elseif (isset($user) && isset($_GET['edit_vote']) && $theme->reason_close == NULL && $user['id'] == $theme->id_user) {
        include_once 'action/edit_vote.php'; // Редактирование голосования.
    } elseif (isset($user) && isset($_GET['report'])) {
        if ($my_report != 0 || user_access('forum_them_edit') || $user['id'] == $theme->id_user || $theme->reason_close != NULL) {
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
            exit;
        } else {
            if (isset($_POST['send'])) {
                $text = mysql_real_escape_string(trim($_POST['text']));
                if (mb_strlen($text) < 5) {
                    ?>
                    <div class = 'err'>Слишком короткая причина. Указывайте нормальную причину, если не хотите быть забаненым администрацией.</div>
                    <?
                } else {
                    $persons = mysql_query('SELECT `id`, `group_access` FROM `user` WHERE `group_access` > "2"');
                    while ($person = mysql_fetch_object($persons)) {
                        $access = mysql_result(mysql_query('SELECT COUNT(*) FROM `user_group_access` WHERE `id_group` = '.$person->group_access.' AND `id_access` = "forum_them_edit"'), 0);
                        if ($access != 0) {
                            $msg = 'Пользователь [url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] подал жалобу на тему [url='.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html]'.$theme->name.'[/url].
                            Причина указана тут - [url='.FORUM.'/reports.html]ссылка[/url]';
                            mysql_query('INSERT INTO `mail` SET `id_user` = "0", `id_kont` = '.$person->id.', `msg` = "'.mysql_real_escape_string($msg).'", `time` = '.$time);
                        }
                    }
                    mysql_query('INSERT INTO `forum_reports` SET `id_theme` = '.$theme->id.', `id_user` = '.$user['id'].', `text` = "'.$text.'"');
                    $_SESSION['success'] = '<div class = "msg">Жалоба успешно отправлена на рассмотрение администрацией.</div>';
                    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
                    exit;
                }
            }
            ?>
            <form method = 'post' action = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/report.html' class="p_m">
                <b>Укажите причину:</b><br />
                <textarea name = 'text' style = 'width: 92%; height: 50px'></textarea><br />
                <input type = 'submit' name = 'send' value = 'Отправить жалобу' />
            </form>
            <?
            include_once '../sys/inc/tfoot.php';
            exit;
        }
    }

    $my_voice = (isset($vote) && isset($user)) ? mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_vote_voices` WHERE `id_user` = '.$user['id'].' AND `id_vote` = '.$vote->id), 0) : NULL;

    if (isset($user)) {
        $last_post = mysql_result(mysql_query('SELECT MAX(`id`) FROM `forum_posts` WHERE `id_theme` = '.$theme->id), 0);
        if (isset($_GET['delete_post']) && user_access('forum_post_ed')) {
            $_SESSION['success'] = '<div class = "msg">Комментарий успешно удалён.</div>';
            $files = mysql_query('SELECT `name` FROM `forum_post_files` WHERE `id_post` = '.intval($_GET['delete_post']));
            while ($file = mysql_fetch_object($files)) {
                unlink(H.FORUM.'/files/'.$file->name);
            }
            mysql_query('DELETE FROM `forum_post_rating` WHERE `id_post` = '.intval($_GET['delete_post']));
            mysql_query('DELETE FROM `forum_posts` WHERE `id` = '.intval($_GET['delete_post']));
            if ($k_post == 0) {
                mysql_query('UPDATE `forum_themes` SET `time_post` = '.$theme->time.' WHERE `id` = '.$theme->id);
            }
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
            exit;
        } elseif (isset($_GET['hide']) && user_access('forum_post_ed')) {
            $_SESSION['success'] = '<div class = "msg">Комментарий успешно скрыт.</div>';
            mysql_query('UPDATE `forum_posts` SET `id_admin` = '.$user['id'].', `hide` = "1" WHERE `id` = '.intval($_GET['hide']));
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
            exit;
        } elseif (isset($_GET['display']) && user_access('forum_post_ed')) {
            $_SESSION['success'] = '<div class = "msg">Комментарий успешно показан вновь.</div>';
            mysql_query('UPDATE `forum_posts` SET `id_admin` = "0", `hide` = "0" WHERE `id` = '.intval($_GET['display']));
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
            exit;
        } elseif (isset($_GET['vote']) && $my_voice == 0) {
            $_SESSION['success'] = '<div class = "msg">Ваш голос успешно учтён.</div>';
            mysql_query('INSERT INTO `forum_vote_voices` SET `id_vote` = '.$vote->id.', `id_variant` = '.intval($_GET['vote']).', `id_user` = '.$user['id']);
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
            exit;
        } elseif (isset($_GET['del_file']) && (mysql_fetch_object(mysql_query('SELECT `id_user` FROM `forum_post_files` WHERE `id` = '.intval($_GET['del_file'])))->id_user == $user['id'] || user_access('forum_post_ed'))) {
            $_SESSION['success'] = '<div class = "msg">Файл успешно удалён.</div>';
            $file = mysql_fetch_object(mysql_query('SELECT `id`, `id_post`,  `name` FROM `forum_post_files` WHERE `id` = '.intval($_GET['del_file'])));
            unlink(H.FORUM.'/files/'.$file->name);
            $id_post = $file->id_post;
            unset($_SESSION['sort_'.$user['id']]);
            $count = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_posts` WHERE `id_theme` = '.$theme->id.' AND `id` < '.($id_post+1)), 0);
            $count_posts = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_posts` WHERE `id_theme` = '.$theme->id), 0);
            $this_pages = k_page($count, $set['p_str']);
            $pages = ($this_pages > 0) ? '/page='.$this_pages : '.html';
            mysql_query('DELETE FROM `forum_post_files` WHERE `id` = '.$file->id);
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.$pages.'#'.$id_post);
            exit;
        }
        if (isset($_POST['comment']) && ($theme->reason_close == NULL || ($theme->reason_close != NULL && user_access('forum_post_close')))) {
            $text = mysql_real_escape_string($_POST['text']);
            if (isset($_POST['translit']) && $_POST['translit'] == 1) {
                $text = translit($text);
            }
            $mat = antimat($text);
            if ($mat) {
                ?>
                <div class = 'err'>В тексте сообщения обнаружен мат: <?= $mat ?>.</div>
                <?
            } elseif (mb_strlen($text) < $set['forum_new_them_min'] || mb_strlen($text) > $set['forum_new_them_max']) {
                ?>
                <div class = 'err'>Комментарий может содержать от <?= $set['forum_new_them_min'] ?> до <?= $set['forum_new_them_max'] ?> символов.</div>
                <?
            } elseif (mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_posts` WHERE `id_user` = '.$user['id'].' AND `text` = "'.$text.'"'), 0) != 0) {
                ?>
                <div class = 'err'>Вы уже писали это в данной теме. Будьте оригинальнее.</div>
                <?
            } else {
                if (isset($_GET['cit']) && is_numeric($_GET['cit'])) {
                    $cit = intval($_GET['cit']);
                    $privat = 0;
                } elseif (isset($_GET['privat']) && is_numeric($_GET['privat'])) {
                    $privat = intval($_GET['privat']);
                    $cit = 0;
                } else {
                    $cit = 0;
                    $privat = 0;
                }
                mysql_query('UPDATE `forum_themes` SET `time_post` = '.$time.' WHERE `id` = '.$theme->id);
                mysql_query('INSERT INTO `forum_posts` SET `id_theme` = '.$theme->id.', `id_user` = '.$user['id'].', `id_admin` = "0", `text` = "'.$text.'", `cit` = '.$cit.', `privat` = '.$privat.', `time` = "'.$time.'", `hide` = "0"');
                $post_id = mysql_insert_id();
                mysql_query('UPDATE `user` SET `balls` = "'.($user['balls']+$set['forum_balls_add_post']).'" WHERE `id` = '.$user['id']);

				// Вычисляем рейтинг за сообщение.
				$rating_add_msg = rating :: msgnum($text);
				// Суммируем.
				$add_rating = ($rating_add_msg + 0.02);
				// Добавляем рейтингю
				rating :: add($user['id'], $add_rating, 1, null, 'forum');


                $count = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_posts` WHERE `id_theme` = '.$theme->id.' AND `id` < '.($post_id+1)), 0);
                $count_posts = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_posts` WHERE `id_theme` = '.$theme->id), 0);
                $this_pages = k_page($count, $set['p_str']);
                $pages = ($this_pages > 0) ? '/page='.$this_pages : '.html';
                $j_f = 'Пользователь [url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] написал в теме "[url='.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.$pages.'#'.$post_id.']'.$theme->name.'[/url]", за которой Вы следите.
                Комментарий: [b]'.$text.'[/b]';
                $forum_js = mysql_query('SELECT `id_user` FROM `forum_journal` WHERE `id_theme` = '.$theme->id);
                while ($forum_j = mysql_fetch_object($forum_js)) {
                    if (mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_posts` WHERE `id` = '.$post_id.' AND `id_user` = '.$user['id']), 0) == 0 && ((isset($_GET['answer']) && $_GET['answer'] != $forum_j->id_user) || !isset($_GET['answer']))) {
                        mysql_query('INSERT INTO `f_journal` SET `id_user` = '.$forum_j->id_user.', `type` = "themes", `text` = "'.mysql_real_escape_string($j_f).'", `time` = "'.$time.'"');
                    }
                }
                if (!isset($_GET['cit']) && !isset($_GET['privat']) && isset($_GET['answer'])) {
                    $j = 'Пользователь [url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] ответил Вам в теме "[url='.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.$pages.'#'.$post_id.']'.$theme->name.'[/url]".
                    Ответ: [b]'.$text.'[/b]';
                    $type = 'answers';
                } elseif (isset($_GET['cit']) && isset($_GET['answer'])) {
                    $j = 'Пользователь [url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] процитировал Ваш комментарий в теме [url='.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.$pages.'#'.$post_id.']'.$theme->name.'[/url].
                    Цитата: [b]'.$text.'[/b]';
                    $type = 'quotes';
                } elseif (isset($_GET['privat'])) {
                    $j = 'Пользователь [url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] оставил Вам приватное сообщение в теме [url='.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.$pages.'#'.$post_id.']'.$theme->name.'[/url].
                    Сообщение: [b]'.$text.'[/b]';
                    $type = 'privat';
                }
                if (isset($user) && $user['id'] != $theme->id_user && !isset($_GET['answer']) && !isset($_GET['cit']) && !isset($_GET['privat'])) {
                    $j_t = 'Пользователь [url=/info.php?id='.$user['id'].']'.$user['nick'].'[/url] написал комментарий в Вашей теме "[url='.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.$pages.'#'.$post_id.']'.$theme->name.'[/url]".
                    Комментарий: [b]'.$text.'[/b]';
                    mysql_query('INSERT INTO `f_journal` SET `id_user` = '.$theme->id_user.', `type` = "my_themes", `text` = "'.mysql_real_escape_string($j_t).'", `time` = "'.$time.'"');
                }
                if (isset($_GET['answer'])) {
                    mysql_query('INSERT INTO `f_journal` SET `id_user` = '.intval($_GET['answer']).', `type` = "'.$type.'", `text` = "'.mysql_real_escape_string($j).'", `time` = "'.$time.'"');
                } elseif (isset($_GET['privat'])) {
                    mysql_query('INSERT INTO `f_journal` SET `id_user` = '.intval($_GET['privat']).', `type` = "'.$type.'", `text` = "'.mysql_real_escape_string($j).'", `time` = "'.$time.'"');
                }
                $_SESSION['success'] = '<div class = "msg">Комментарий успешно добавлен.</div>';
                $post = mysql_fetch_object(mysql_query('SELECT `id` FROM `forum_posts` WHERE `id` = '.$post_id));
                if (isset($_POST['add_file']) && $post) {
                    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/'.$post->id.'/add_file');
                } else {
                    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
                }
                exit;
            }
        } elseif (isset($user) && isset($_POST['edit_post'])) {
            $edit_post = mysql_real_escape_string(trim($_POST['post']));
            $post_edit = mysql_fetch_object(mysql_query('SELECT `count_edit` FROM `forum_posts` WHERE `id` = '.intval($_POST['id_post'])));
            mysql_query('UPDATE `forum_posts` SET `text` = "'.$edit_post.'", `last_edit` = '.$time.', `who_edit` = "'.$user['nick'].'", `count_edit` = '.($post_edit->count_edit+1).' WHERE `id` = '.intval($_POST['id_post']));
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html#'.intval($_POST['id_post']));
            exit;
        } elseif (isset($_POST['cancel_edit'])) {
            header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
            exit;
        } elseif (isset($user) && isset($_GET['act_posts']) && (isset($_POST['delete_posts']) || isset($_POST['hide_posts']) || isset($_POST['display_posts']))) {
            include_once 'action/actions_posts.php'; // Действия над постами.
        }
    }
    if (isset($user) && isset($vote) && mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_votes_var` WHERE `variant` = "" AND `id_vote` = '.$vote->id), 0) != 0) {
        mysql_query('DELETE FROM `forum_votes_var` WHERE `id_vote` = '.$vote->id);
        mysql_query('DELETE FROM `forum_votes` WHERE `id` = '.$vote->id);
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        exit;
    }
    if (isset($user) && isset($_GET['journal_yes'])) {
        if (mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_journal` WHERE `id_theme` = '.$theme->id.' AND `id_user` = '.$user['id']), 0) == 0) {
            mysql_query('INSERT INTO `forum_journal` SET `id_theme` = '.$theme->id.', `id_user` = '.$user['id']);
            $_SESSION['success'] = '<div class = "msg">Вы успешно подписались на эту тему.</div>';
        }
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        exit;
    } elseif (isset($user) && isset($_GET['journal_no'])) {
        if (mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_journal` WHERE `id_theme` = '.$theme->id.' AND `id_user` = '.$user['id']), 0) != 0) {
            mysql_query('DELETE FROM `forum_journal` WHERE `id_theme` = '.$theme->id.' AND `id_user` = '.$user['id']);
            $_SESSION['success'] = '<div class = "msg">Вы успешно отписались от этой темы.</div>';
        }
        header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
        exit;
    }
    if (isset($_SESSION['success'])) {
        echo $_SESSION['success'];
        unset($_SESSION['success']);
    }
    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?= FORUM ?>'>Форум</a> / <a href = '<?= FORUM.'/'.$forum->id ?>/'><?= output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?= output_text($razdel->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?= output_text($theme->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?
    if ($theme->reason_close != NULL) {
        ?>
        <div class = 'err'><?= $theme->reason_close ?></div>
        <?
    }

    $person = mysql_fetch_object(mysql_query('SELECT `id`, `nick`,`mylink`, `pol` FROM `user` WHERE `id` = '.$theme->id_user));

    $k_page = k_page($k_post, $set['p_str']);
    $page = page($k_page);
    $start = $set['p_str']*$page-$set['p_str'];
    ?>
    <table class = 'post'>
        <tr>
            <td class = 'icon14'>
                <img src = '<?= FORUM ?>/icons/gr.png' alt = '' <?= ICONS ?> />
            </td>

            <td class = 'p_t'>
				<a href = '/<?=$person->mylink;?>'> <?=nick($person->id)?> </a> (<?= vremja($theme->time) ?>)
			</td>

        </tr>
        <tr>
            <td class = 'p_m' colspan = '2' style = 'word-wrap: break-word'>
                <?= output_text($theme->description) ?>
            </td>
        </tr>
        <?
        if (isset($vote)) {
            $all_votes = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_vote_voices` WHERE `id_vote` = '.$vote->id), 0);
            ?>
            <tr>
                <td class = 'p_m' colspan = '2'>
                    <?
                    $i = 0;
                    echo output_text($vote->name).'<br />';
                    $vars = mysql_query('SELECT `id`, `variant` FROM `forum_votes_var` WHERE `id_vote` = '.$vote->id.' ORDER BY `id` ASC');
                    while ($var = mysql_fetch_object($vars)) {
                        $i++;
                        $vote_var = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_vote_voices` WHERE `id_vote` = '.$vote->id.' AND `id_variant` = '.$var->id), 0);
                        $procent = ($all_votes == 0) ? 0 : $vote_var/$all_votes*100;
                        $procent = sprintf("%u", $procent);
                        echo output_text($var->variant).' ('.$procent.'%) - '.$vote_var.' чел.';
                        if (isset($user) && $my_voice == 0) {
                            ?>
                            <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/vote=<?= $var->id ?>'>
                            <?
                        }
                        ?>
                        <div class = 'votes'>
                            <!--img src = '<?= FORUM ?>/icons/votes/poll<?= $i ?>.gif' alt = '<?= $i ?>' style = 'height: 13px; width: <?= $procent ?>%; <?= ($procent > 0) ? 'border-right: 1px solid black;' : NULL ?>' /-->
                            <span style="background: url('<?= FORUM ?>/icons/votes/poll<?= $i ?>.gif') repeat-x;width: <?= $procent ?>%;"></span>
                        </div>
                        <?
                        if (isset($user) && $my_voice == 0) {
                            echo '</a>';
                        }
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td class = 'p_m' colspan = '2'>
                    Начало голосования: <?= vremja($vote->time) ?><br />
                    <?
                    if ($vote->time_end > time()) {
                        echo 'Завершение голосования: '.vremja($vote->time_end).'<br />';
                    } elseif ($vote->time_end < time() && $vote->time_end != 0) {
                        echo 'Голосование завершено.<br />';
                    }
                    ?>
                    Проголосовало: <?= $all_votes ?> чел.
                </td>
            </tr>
            <?
        }
        if (isset($user) && $my_report == 0 && !user_access('forum_them_edit') && $user['id'] != $theme->id_user && $theme->reason_close == NULL) {
            ?>
            <tr>
                <td class = 'p_m' colspan = '2' style = 'text-align: right'>
                    <img src = '<?= FORUM ?>/icons/report.png' alt = '' style = 'width: 16px; height: 16px' /> <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/report.html'>Жалоба на тему</a>
                </td>
            </tr>
            <?
        }
        ?>
    </table>
    <?
    if ($theme->time_edit != 0 && $theme->id_admin != 0) {
        $admin = mysql_fetch_object(mysql_query('SELECT `id`, `nick`,`mylink` FROM `user` WHERE `id` = '.$theme->id_admin));
        ?>
        <div class = 'p_t'>Последний раз редактировалось
		<a href = '/<?=$admin->mylink;?>'>
		<?=nick($admin->id,null,0,1)?>
		</a> (<?= vremja($theme->time_edit) ?>)</div>
        <?
    }
    ?>
    <div class = 'p_m'>
        Комментарии (<?= $k_post ?>)<br />
        Новые: <?= (!isset($_SESSION['sort_'.$user['id']])) ? '<b>Вверху</b> | <a href = "'.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/sort_1">Внизу</a>' : '<a href = "'.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/sort_0">Вверху</a> | <b>Внизу</b>' ?>
        <?
        if (isset($user) && $user['id'] != $theme->id_user) {
            $journal = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_journal` WHERE `id_theme` = "'.$theme->id.'" AND `id_user` = '.$user['id']), 0);
            $theme_journal = ($journal == 0) ? '<a href = "'.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/journal_yes">Да</a> | Нет' : 'Да | <a href = "'.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/journal_no">Нет</a>';
            ?>
            <div style = 'text-align: right'>Следить за темой: <?= $theme_journal ?></div>
            <?
        }
        ?>
    </div>
    <?
    if ($k_post == 0) {
        ?>
        <div class = 'err'>Никто ещё не комментировал эту тему.</div>
        <?
    }
    if (isset($user) && ($theme->reason_close == NULL || ($theme->reason_close != NULL && user_access('forum_post_close')))) {
        $cit = 0; $privat = 0; $text = '';

        if (isset($_GET['cit'])) {
            $cit = intval($_GET['cit']);
            $answer = intval($_GET['answer']);
            $text = mysql_fetch_object(mysql_query('SELECT `id`, `nick` FROM `user` WHERE `id` = '.$answer))->nick.', ';
            ?>
            <form method = 'post' action = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/cit=<?= $cit ?>/answer=<?= $answer ?>' class="p_m">
            <?
        } elseif (isset($_GET['privat'])) {
            $privat = intval($_GET['privat']);
            $text = mysql_fetch_object(mysql_query('SELECT `id`, `nick` FROM `user` WHERE `id` = '.$privat))->nick.', ';
            ?>
            <form method = 'post' action = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/privat=<?= $privat ?>' class="p_m">
            <?
        } elseif (isset($_GET['answer'])) {
            $answer = intval($_GET['answer']);
            $text = mysql_fetch_object(mysql_query('SELECT `id`, `nick` FROM `user` WHERE `id` = '.$answer))->nick.', ';
            ?>
            <form method = 'post' action = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/answer=<?= $answer ?>' class="p_m">
            <?
        } else {
            ?>
            <form method = 'post' action = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html' class="p_m">
            <?
        }
        $cit = mysql_fetch_object(mysql_query('SELECT `text` FROM `forum_posts` WHERE `id` = '.$cit));
        if (isset($_GET['cit']) && isset($_GET['answer'])) {
            ?>
            <div class = 'cit'>Цитирование сообщения: "<?= output_text($cit->text) ?>"</div>
            <?
        }
        ?>
        <textarea name = 'text' style = 'width: 92%; height: 50px'><?= $text ?></textarea><br />
        <?
        if ($user['set_translit'] == 1) {
            ?>
            <label><input type = 'checkbox' name = 'translit' value = '1' /> Транслит</label><br />
            <?
        }
        if ($user['set_files'] == 1) {
            ?>
            <label><input type = 'checkbox' name = 'add_file' value = '1' /> Добавить файл</label><br />
            <?
        }
        ?>
        <input type = 'submit' name = 'comment' value = 'Отправить' />
        </form>
        <?
    }
    if (isset($_GET['act_posts'])) {
        ?>
        <form action = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/act_posts' method = 'post' style = 'padding: 0px'>
        <?
    }
    ?>
    <table class = 'post'>
        <?
        $post_count = $start;
        $sort = (!isset($_SESSION['sort_'.$user['id']])) ? 'DESC' : 'ASC';
        $limit = (isset($_GET['act_posts'])) ? NULL : ' LIMIT '.$start.', '.$set['p_str'];
        $posts = mysql_query('SELECT * FROM `forum_posts` WHERE `id_theme` = '.$theme->id.' ORDER BY `id` '.$sort.$limit);

        while ($post = mysql_fetch_object($posts)) {

            $man = ($post->id_user != 0) ? mysql_fetch_object(mysql_query('SELECT `id`, `nick`, `pol` FROM `user` WHERE `id` = '.$post->id_user)) : array();
            $man_id = ($man && $post->id_user != 0) ? $man->id : 0;
            $man_pol = ($man && $post->id_user != 0) ? $man->pol : 0;
            $man_nick = ($man && $post->id_user != 0) ? $man->nick : 'Система';

            $privat = ($post->privat == $user['id'] || ($post->id_user == $user['id'] && $post->privat != 0)) ? '<span style = "color: blue">Приватно</span>' : NULL;
            $admin = ($post->hide == 1) ? mysql_fetch_object(mysql_query('SELECT `id`, `nick`, `pol` FROM `user` WHERE `id` = '.$post->id_admin)) : NULL;

            if ($post->privat == 0 || ($post->privat == $user['id'] || $post->id_user == $user['id']) || $user['group_access'] > 14) {
                $post_count++;
                echo '<a id = "'.$post->id.'"></a>';
                ?>
                <tr>
                    <td class = 'icon14'>
                       #<?=$post_count ?>
                    </td>
                    <td class = 'p_t'>
                        <?
                        if (isset($_GET['act_posts'])) {
                            ?>
                            <input type = 'checkbox' name = 'act[]' value = '<?= $post->id ?>' />
                            <?
                        }
                        ?>
                       <?=nick($man_id)?> (<?= vremja($post->time) ?>) <?= $privat ?>

				   </td>
                </tr>
                <tr>
                    <td class = 'p_m' colspan = '2'>
                        <?
                        if (isset($_GET['edit']) && $_GET['edit'] == $post->id) {
                            $edit_post = mysql_fetch_object(mysql_query('SELECT `id`, `id_user`, `text` FROM `forum_posts` WHERE `id` = '.intval($_GET['edit'])));
                            if (isset($user) && (user_access('forum_post_ed') || ((time()-$post->time < $set['forum_edit_post_time']) && $theme->reason_close == NULL && $user['id'] == $edit_post->id_user && $last_post == $edit_post->id))) {
                                ?>
                                <form action = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html' method = 'post'>
                                    <input type = 'hidden' name = 'id_post' value = '<?= intval($_GET['edit']) ?>' />
                                    <textarea name = 'post' style = 'width: 92%; height: 50px'><?= $edit_post->text ?></textarea><br />
                                    <input type = 'submit' name = 'edit_post' value = 'Изменить' /> <input type = 'submit' name = 'cancel_edit' value = 'Отмена' />
                                </form>
                                <?
                            } else {
                                header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
                                exit;
                            }
                        } else {
                            if ($post->hide == 1) {
                                ?>
                                <span style = 'color: Firebrick'><b>Комментарий скрыл<?= ($admin->pol == 0) ? 'а' : NULL ?> <a href = '/info.php?id=<?= $admin->id ?>'><?= $admin->nick ?></a></b></span>
                                <?
                                if (user_access('forum_post_ed')) {
                                    echo '<br /><s>'.output_text($post->text).'</s><br />';
                                }
                            } else {
                                if (mysql_result(mysql_query('SELECT MAX(`rating`) FROM `forum_posts` WHERE `rating` > '.RATING.' AND `id_theme` = '.$theme->id), 0) == $post->rating) {
                                    ?>
                                    <span style = 'float: right; padding-left: 10px; color: <?= COLOR ?>'>
                                        Лучший ответ
                                    </span>
                                    <?
                                }
                                if ($post->cit != 0 && mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_posts` WHERE `id` = '.$post->cit), 0) != 0) {
                                    $cit_post = mysql_fetch_object(mysql_query('SELECT `id_user`, `text`, `time` FROM `forum_posts` WHERE `id` = '.$post->cit));
                                    $user_cit = mysql_fetch_object(mysql_query('SELECT `id`, `nick` FROM `user` WHERE `id` = '.$cit_post->id_user));
                                    ?>
                                    <div class = 'cit'>
                                        <b><?= $user_cit->nick ?> (<?= vremja($cit_post->time) ?>):</b><br />
                                        <?= output_text($cit_post->text) ?>
                                    </div>
                                    <?
                                }
                                echo output_text($post->text).'<br />';
                            }
                        }
                        if ($post->hide == 0 || ($post->hide == 1 && user_access('forum_post_ed'))) {
                            $files = mysql_query('SELECT * FROM `forum_post_files` WHERE `id_post` = '.$post->id);
                            if (mysql_num_rows($files) != 0) {
                                echo '<br /><b>Прикреплённые файлы:</b><br />';
                                while ($file = mysql_fetch_object($files)) {
                                    $ras = strtolower(preg_replace('#^.*\.#', NULL, $file->name));
                                    if ($ras == 'jpg' || $ras == 'jpeg' || $ras == 'gif' || $ras == 'png' || $ras == 'bmp' || $ras == 'ico') {
                                        $icon = FORUM.'/files/'.$file->name;
                                    } elseif ($ras == '3gp' || $ras == 'mp4' || $ras == 'avi' || $ras == 'mpeg' || $ras == 'flv' || $ras == 'wmv' || $ras == 'mkv') {
                                        $icon = FORUM.'/icons/files/video.png';
                                    } elseif ($ras == 'docx' || $ras == 'doc' || $ras == 'docm' || $ras == 'dotx' || $ras == 'dot' || $ras == 'dotm') {
                                        $icon = FORUM.'/icons/files/word.png';
                                    } elseif ($ras == 'mp1' || $ras == 'mp2' || $ras == 'mp3' || $ras == 'wav' || $ras == 'aif' || $ras == 'ape' || $ras == 'flac' || $ras == 'ogg' || $ras == 'asf' || $ras == 'wma') {
                                        $icon = FORUM.'/icons/files/music.png';
                                    } elseif ($ras == 'zip' || $ras == 'rar' || $ras == 'tar' || $ras == '7-zip' || $ras == 'gzip' || $ras == 'jar' || $ras == 'jad' || $ras == 'war' || $ras == 'xar') {
                                        $icon = FORUM.'/icons/files/archive.png';
                                    } elseif ($ras == 'txt' || $ras == 'xml') {
                                        $icon = FORUM.'/icons/files/txt.png';
                                    } elseif ($ras == 'pdf') {
                                        $icon = FORUM.'/icons/files/pdf.png';
                                    } elseif ($ras == 'psd') {
                                        $icon = FORUM.'/icons/files/psd.png';
                                    } else {
                                        $icon = FORUM.'/icons/files/file.png';
                                    }
                                    ?>
                                    <table style = 'width: 100%' cellspacing = '0' cellpadding = '0' border = '0'>
                                        <tr>
                                            <td class = 'p_t' style = 'width: 50px; border-right: none'>
                                                <img src = '<?= $icon ?>' alt = '' style = 'width: 50px; height: 50px;' />
                                            </td>
                                            <td class = 'p_t' style = 'border-left: none'>
                                                <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/download_<?= $file->id ?>'>
                                                    <?= output_text($file->real_name, 1, 1, 0, 0, 0) ?>
                                                </a> (<?= size_file($file->size) ?>)<br />
                                                Скачали: <?= $file->count_downloads ?> чел.
                                                <?
                                                if ($user['id'] == $post->id_user || user_access('forum_post_ed')) {
                                                    ?>
                                                    <br />[ <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/del_file='.$file->id ?>'>Удалить файл</a> ]
                                                    <?
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    </table>
                                    <?
                                }
                                echo '<br />';
                            }
                        }
                        if ($post->count_edit != 0) {
                            echo '('.$post->count_edit.') Посл. ред. '.vremja($post->last_edit).' - '.$post->who_edit.'<br />';
                        }
                        if (isset($user) && ((isset($_GET['edit']) && $post->id != $_GET['edit']) || !isset($_GET['edit']))) {
                            if ($user['id'] != $post->id_user && $post->id_user != 0 && $post->hide == 0 && ($theme->reason_close == NULL || ($theme->reason_close != NULL && user_access('forum_post_close')))) {
                                ?>
                                <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/answer=<?= $man_id ?>'>Отв</a>
                                 | <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/cit=<?= $post->id ?>/answer=<?= $man_id ?>'>Цит</a>
                                 | <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/privat=<?= $man_id ?>'>Прив</a>
                                <?
                            }
                            if ($post->id_user != 0 && (user_access('forum_post_ed') || ((time()-$post->time < $set['forum_edit_post_time']) && $theme->reason_close == NULL && $user['id'] == $post->id_user && $last_post == $post->id))) {
                                $razd =  (user_access('forum_post_ed') && $post->hide == 0 && $post->id_user != $user['id']) ? ' | ' : NULL;
                                echo $razd.'<a href = "'.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/edit_'.$post->id.'#'.$post->id.'">Ред</a>';
                            }
                            if (user_access('forum_post_ed')) {
                                $razd = ($post->id_user == 0) ? NULL : ' | ';
                                echo $razd;
                                ?>
                                <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/delete_post=<?= $post->id ?>'>Удал</a>
                                <?
                                if ($post->hide == 0) {
                                    ?>
                                     | <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/hide=<?= $post->id ?>'>Скрыть</a>
                                    <?
                                } else {
                                    ?>
                                     | <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/display=<?= $post->id ?>'>Показ</a>
                                    <?
                                }
                            }
                        }
                        if ($user['id'] != $post->id_user && $post->id_user != 0) {
                            $_post = (isset($_GET['post'])) ? mysql_fetch_object(mysql_query('SELECT `id`, `rating`, `text` FROM `forum_posts` WHERE `id` = '.intval($_GET['post']))) : NULL;
                            if (isset($user) && isset($_GET['like']) && mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_post_rating` WHERE `id_user` = '.$user['id'].' AND `id_post` = '.intval($_GET['post']).' AND `type` = "0"'), 0) == 0) {
                                $plus = (mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_post_rating` WHERE `id_user` = '.$user['id'].' AND `id_post` = '.intval($_GET['post']).' AND `type` = "1"'), 0) != 0) ? 2 : 1;
                                if (mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_post_rating` WHERE `id_user` = '.$user['id'].' AND `id_post` = '.intval($_GET['post'])), 0) != 0) {
                                    mysql_query('UPDATE `forum_post_rating` SET `type` = "0" WHERE `id_post` = '.intval($_GET['post']).' AND `id_theme` = '.$theme->id.' AND `id_user` = '.$user['id']);
                                } else {
                                    mysql_query('INSERT INTO `forum_post_rating` SET `id_theme` = '.$theme->id.', `id_user` = '.$user['id'].', `type` = "0", `id_post` = '.intval($_GET['post']));
                                }
                                mysql_query('UPDATE `forum_posts` SET `rating` = '.($_post->rating+$plus).' WHERE `id` = '.intval($_GET['post']));
                                $_SESSION['success'] = '<div class = "msg">Вы успешно проголосвали за пост.</div>';
                                $page = (is_numeric($_GET['page'])) ? intval($_GET['page']) : mysql_real_escape_string(trim($_GET['page']));
                                if ($page != 'end' && !is_numeric($page)) {
                                    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
                                } else {
                                    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/page='.$page.'#'.intval($_GET['post']));
                                }
                                exit;
                            } elseif (isset($user) && isset($_GET['dislike']) && mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_post_rating` WHERE `id_user` = '.$user['id'].' AND `id_post` = '.intval($_GET['post']).' AND `type` = "1"'), 0) == 0) {
                                $minus = (mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_post_rating` WHERE `id_user` = '.$user['id'].' AND `id_post` = '.intval($_GET['post']).' AND `type` = "0"'), 0) != 0) ? 2 : 1;
                                if (mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_post_rating` WHERE `id_user` = '.$user['id'].' AND `id_post` = '.intval($_GET['post'])), 0) != 0) {
                                    mysql_query('UPDATE `forum_post_rating` SET `type` = "1" WHERE `id_post` = '.intval($_GET['post']).' AND `id_theme` = '.$theme->id.' AND `id_user` = '.$user['id']);
                                } else {
                                    mysql_query('INSERT INTO `forum_post_rating` SET `id_theme` = '.$theme->id.', `id_user` = '.$user['id'].', `type` = "1", `id_post` = '.intval($_GET['post']));
                                }
                                if (($_post->rating-$minus) < RATING2) {
                                    $persons = mysql_query('SELECT `id`, `group_access` FROM `user` WHERE `group_access` > "2"');
                                    while ($person = mysql_fetch_object($persons)) {
                                        $access = mysql_result(mysql_query('SELECT COUNT(*) FROM `user_group_access` WHERE `id_group` = '.$person->group_access.' AND `id_access` = "forum_post_ed"'), 0);
                                        if ($access != 0) {
                                            $msg = 'Комментарий в теме [url='.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/page='.$page.'#'.$_post->id.']'.$theme->name.'[/url] набрал много отрицательных отзывов. Есть подозрение, что он нарушает правила форума.
                                            [b]Сам комментарий:[/b]
                                            '.output_text($_post->text);
                                            mysql_query('INSERT INTO `mail` SET `id_user` = "0", `id_kont` = '.$person->id.', `msg` = "'.mysql_real_escape_string($msg).'", `time` = '.$time);
                                        }
                                    }
                                }
                                mysql_query('UPDATE `forum_posts` SET `rating` = '.($_post->rating-$minus).' WHERE `id` = '.intval($_GET['post']));
                                $_SESSION['success'] = '<div class = "msg">Вы успешно проголосвали за пост.</div>';
                                $page = (is_numeric($_GET['page'])) ? intval($_GET['page']) : mysql_real_escape_string(trim($_GET['page']));
                                if ($page != 'end' && !is_numeric($page)) {
                                    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'.html');
                                } else {
                                    header('Location: '.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/page='.$page.'#'.intval($_GET['post']));
                                }
                                exit;
                            }
                            ?>
                            <span style = 'float: right; padding-left: 5px'>
                                <?
                                if (isset($user) && mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_post_rating` WHERE `id_user` = '.$user['id'].' AND `id_post` = '.$post->id.' AND `type` = "0"'), 0) == 0) {
                                    ?>
                                    <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/page='.$page.'/'.$post->id.'/like' ?>'><img src = '<?= FORUM ?>/icons/like.png' alt = '' /></a>
                                    <?
                                } else {
                                    ?>
                                    <img src = '<?= FORUM ?>/icons/like.png' alt = '' />
                                    <?
                                }
                                echo '('.$post->rating.')';
                                if (isset($user) && mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_post_rating` WHERE `id_user` = '.$user['id'].' AND `id_post` = '.$post->id.' AND `type` = "1"'), 0) == 0) {
                                    ?>
                                    <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/page='.$page.'/'.$post->id.'/dislike' ?>'><img src = '<?= FORUM ?>/icons/dislike.png' alt = '' /></a>
                                    <?
                                } else {
                                    ?>
                                    <img src = '<?= FORUM ?>/icons/dislike.png' alt = '' />
                                    <?
                                }
                                ?>
                            </span>
                            <?
                        } else {
                            ?>
                            <span style = 'float: right; padding-left: 5px'>
                                (<?= $post->rating ?>)
                            </span>
                            <?
                        }
                        ?>
                    </td>
                </tr>
                <?
            }
        }
        ?>
    </table>
    <?
    if (isset($_GET['act_posts'])) {
        ?>
        <input type = 'submit' name = 'delete_posts' value = 'Удалить' /> <input type = 'submit' name = 'hide_posts' value = 'Скрыть' /> <input type = 'submit' name = 'display_posts' value = 'Показать' />
        </form>
        <?
    }
    if ($k_page > 1) {
        str(FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'/', $k_page, $page);
    }
    $who = mysql_result(mysql_query('SELECT COUNT(*) FROM `user` WHERE `date_last` > "'.(time()-600).'" AND `url` LIKE "'.FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id.'%"'), 0);
    $count_files = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_post_files` WHERE `id_theme` = '.$theme->id), 0);
    ?>
    <div class = 'p_m'>
        <img src = '<?= FORUM ?>/icons/who.png' alt = '' <?= ICONS ?> /> <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/who.html'>Сейчас в теме</a> [<?= $who ?>]
    </div>
    <div class = 'p_m'>
        <img src = '<?= FORUM ?>/icons/files.png' alt = '' <?= ICONS ?> /> <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/files.html'>Файлы в теме</a> [<?= $count_files ?>]
    </div>
    <?
    if ((user_access('forum_them_edit') || $user['id'] == $theme->id_user) && ($theme->reason_close == NULL || ($theme->reason_close != NULL && user_access('forum_post_close')))) {
        ?>
        <div class = 'p_m'>
            <img src = '<?= FORUM ?>/icons/edit.png' alt = '' <?= ICONS ?> /> <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/edit_theme.html'>Редактировать</a>
        </div>
        <?
        if (mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_votes` WHERE `id_theme` = '.$theme->id), 0) == 0 && ($theme->reason_close == NULL || ($theme->reason_close != NULL && user_access('forum_post_close'))) && $user['id'] == $theme->id_user) {
            ?>
            <div class = 'p_m'>
                <img src = '<?= FORUM ?>/icons/vote.png' alt = '' <?= ICONS ?> /> <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/create_vote.html'>Создать голосование</a>
            </div>
            <?
        } elseif (mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_votes` WHERE `id_theme` = '.$theme->id), 0) != 0 && ($theme->reason_close == NULL || ($theme->reason_close != NULL && user_access('forum_post_close'))) && $user['id'] == $theme->id_user) {
            ?>
            <div class = 'p_m'>
                <img src = '<?= FORUM ?>/icons/vote.png' alt = '' <?= ICONS ?> /> <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/edit_vote.html'>Редактировать голосование</a>
            </div>
            <?
        }
        if (user_access('forum_them_edit')) {
            echo '<div class = "p_m">';
            if ($theme->reason_close == NULL && user_access('forum_post_close')) {
                ?>
                <img src = '<?= FORUM ?>/icons/delete.png' alt = '' <?= ICONS ?> /> <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/close_theme.html'>Закрыть</a>
                <?
            } elseif ($theme->reason_close != NULL && user_access('forum_post_close')) {
                ?>
                <img src = '<?= FORUM ?>/icons/delete.png' alt = '' <?= ICONS ?> /> <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/open_theme'>Открыть</a>
                <?
            }
            if (user_access('forum_post_ed')) {
                ?>
                 | <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/cleare_theme.html'>Очистить</a>
                <?
            }
            echo ' тему</div>';
        }
        if (user_access('forum_post_ed') && user_access('forum_them_edit')) {
            ?>
            <div class = 'p_m'>
                <?
                if (!isset($_GET['act_posts'])) {
                    ?>
                    <img src = '<?= FORUM ?>/icons/edit.png' alt = '' <?= ICONS ?> /> <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>/act_posts'>Действия над постами</a>
                    <?
                } else {
                    ?>
                    <img src = '<?= FORUM ?>/icons/edit.png' alt = '' <?= ICONS ?> /> <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'>Отменить действие</a>
                    <?
                }
                ?>
            </div>
            <?
        }
    }
    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?= FORUM ?>'>Форум</a> / <a href = '<?= FORUM.'/'.$forum->id ?>/'><?= output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id ?>/'><?= output_text($razdel->name, 1, 1, 0, 0, 0) ?></a> / <a href = '<?= FORUM.'/'.$forum->id.'/'.$razdel->id.'/'.$theme->id ?>.html'><?= output_text($theme->name, 1, 1, 0, 0, 0) ?></a>
    </div>
    <?
}

?>